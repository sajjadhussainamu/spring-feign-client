package com.example.training.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.training.dto.EmployeeTrainingResponseDto;
import com.example.training.dto.TrainingResponseDto;

@RestController
@RequestMapping("/trainings")
public class EmployeeTrainingController {

	@GetMapping("")
	public EmployeeTrainingResponseDto getTrainingByEmployeeId(@RequestParam Long employeeId) {

		EmployeeTrainingResponseDto employeeTrainingResponseDto = new EmployeeTrainingResponseDto();
		
		TrainingResponseDto trainingResponseDto = new TrainingResponseDto();
		trainingResponseDto.setId(1l);
		trainingResponseDto.setName("security");
		trainingResponseDto.setStatus("PENDING");
		
		List<TrainingResponseDto> trainingResponseDtos = new ArrayList<>();
		trainingResponseDtos.add(trainingResponseDto);

		employeeTrainingResponseDto.setTrainingResponseDto(trainingResponseDtos);
		employeeTrainingResponseDto.setEmployeeId(employeeId);

		return employeeTrainingResponseDto;
	}

}
