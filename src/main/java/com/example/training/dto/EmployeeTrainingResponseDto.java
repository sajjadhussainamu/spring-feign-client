package com.example.training.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeTrainingResponseDto {

	private Long employeeId;
	List<TrainingResponseDto> trainingResponseDto = new ArrayList<>();

}
