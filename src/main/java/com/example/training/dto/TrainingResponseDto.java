package com.example.training.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TrainingResponseDto {

	private Long id;
	private String name;
	private String status;

}
